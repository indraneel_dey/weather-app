import * as Jquery from 'jquery';
import * as React from 'react';

export interface IWeather {
    id: number,
    main: string,
    description: string,
    icon: string
}


export interface IList {
    dt: number,
    main: {
        temp: number,
        temp_min: number,
        temp_max: number,
        pressure: number,
        sea_level: number,
        grnd_level: number,
        humidity: number,
        temp_kf: number
    },
    wind: {
        speed: number,
        deg: number
    },
    weather: Array<IWeather>,
    rain: {
        "3h": number
    },
    sys: {
        pod: string
    },
    clouds: {
        all: number
    }
    dt_txt: string
}

export interface IFiveDayForecast {
    city: {
        id: number,
        name: string,
        coord: {
            lon: number,
            lat: number
        },
        country: string,
        population: number
    }
    cod: string,
    message: number,
    cnt: number,
    list: Array<IList>
}

export interface IFinalForcast {
    day: string,
    max: number,
    min: number,
    icon: string,
    iconDescription: string
}

export class Forecast extends React.Component<any, any> {
    constructor(props: any) {
        super(props);

        let location: any = props;

        this.state = {
            location: location.location,
            forecast: []
        }
        this.getWeather(this.state.location);
    }



    /*Forecast Information*/
    private forecastUrl: any = {
        baseUrl: "http://api.openweathermap.org/data/2.5/forecast",
        city: this.props.location,
        mode: "json",
        units: "imperial", //or metric, standard gives Kelvin
        appId: "188fd561bdff789714c7409980c32622",
    };

    private buildForecastUrl(location:string) {
        return this.forecastUrl.baseUrl + "?q=" + location
            + "&mode=" + this.forecastUrl.mode + "&units=" + this.forecastUrl.units
            + "&appid=" + this.forecastUrl.appId;
    }


    private groupday(value: Array<IList>) {
        let bydayGroup = [];

        for (let x = 0; x < value.length; x++) {
            let d = new Date(value[x].dt_txt);
            let newDate: string = d.toDateString();//Math.floor(d.getTime() / (1000 * 60 * 60 * 24));
            bydayGroup[newDate] = bydayGroup[newDate] || [];
            bydayGroup[newDate].push(value[x]);
        }

        return bydayGroup;
    }


    private getWeather(location: string) {

        let forcast: IFiveDayForecast;

        Jquery.get(this.buildForecastUrl(location), (data: IFiveDayForecast) => {
            forcast = data;
            let newData = this.groupday(forcast.list);

            let datesFound = Object.keys(newData);
            datesFound.sort((a: string, b: string) => {
                if (new Date(a) > new Date(b)) return 1;
                if (new Date(a) < new Date(b)) return -1;
                return 0;
            })

            if(datesFound[0]==new Date().toDateString()){
                datesFound.shift();
            }


            let finalForcast: Array<IFinalForcast> = [];

            datesFound.forEach((value, index) => {
                let dataSet: Array<IList> = newData[value];

                let indexGet: number = Math.ceil(dataSet.length / 2);

                let forcast: IFinalForcast = {
                    day: value,
                    icon: dataSet[indexGet].weather[0].icon,
                    iconDescription: dataSet[indexGet].weather[0].description,
                    max: Math.max.apply(Math, dataSet.map((o: IList) => {
                        return Math.ceil(o.main.temp_max);
                    })),
                    min: Math.min.apply(Math, dataSet.map((o: IList) => {
                        return Math.ceil(o.main.temp_min);
                    }))
                };
                finalForcast.push(forcast);
            });

            this.setState((p,n)=> {
                    return {
                        location: p.location,
                        forecast: finalForcast
                    };
                }
            )
        });

        return null;
    }

    private getIconUrl(icon: string) {
        return "http://openweathermap.org/img/w/" + icon + ".png";
    }

    public componentWillReceiveProps(nextProps){
        if(nextProps.location !== this.props.location){
            this.getWeather(nextProps.location)
        }
    }

    public render() {
        let monthNames = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"
        ];
        return (
            <article className="container FiveDayWeather">
                <div className="d-flex flex-row">
                    {
                        this.state.forecast.map((o,i) => {

                            let date = new Date(o.day);
                            let day = date.getDate();
                            let month = monthNames[date.getMonth()];
                            let description = o.iconDescription;
                            return (
                                <section className="p-4 forecast" key={i}>
                                    <div className="d-flex flex-column">
                                        <span className="p-2 date">{month} {day}</span>
                                        <span className="p-2 image"><img src={this.getIconUrl(o.icon)} alt={description}
                                                                         title={description}/></span>
                                        <span className="p-2 maxTemp">{o.max}</span>
                                        <span className="p-2 minTemp">{o.min}</span>
                                    </div>
                                </section>
                            );
                        })
                    }
                </div>
            </article>
        );

    }
}