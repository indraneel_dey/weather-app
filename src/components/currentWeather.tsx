import * as Jquery from 'jquery';
import * as React from 'react';

export interface IWeather {
    id:number,
    main:string,
    description:string,
    icon:string
}

export interface ICurrentWeatherJson{
    coord:{
        lon:number,lat:number
    },
    weather:Array<IWeather>,
    base:string,
    main:{
        temp:number,
        pressure:number,
        humidity:number,
        temp_min:number,
        temp_max:number
    },
    wind:{
        speed:number,
        deg:number
    },
    clouds:{
        all:number
    },
    rain:{
        "3h":number
    },
    dt:number,
    sys:{
        type:number,
        id:number,
        message:number,
        country:string,
        sunrise:number,
        sunset:number
    },
    id:number,
    name:string,
    cod:number
}

export interface ICurrentWeather {
    currentTemp: number,
    description: string,
    humidity: number
}

export class CurrentWeather extends React.Component<any,any> {
    constructor(props: any) {
        super(props);
        let location:any = props;

        this.state={
            location: location,
            currentTemp: 0,
            description: "",
            humidity: 0
        }

        this.getWeather(location.location);
    }

    /*Current Weather*/
    public currentUrl:any={
        baseUrl: "http://api.openweathermap.org/data/2.5/weather",
        city: this.props.location,
        mode: "json",
        units: "imperial", //or metric, standard gives Kelvin
        appId: "188fd561bdff789714c7409980c32622",
    };

    private buildCurrentUrl(location:string){
        return this.currentUrl.baseUrl+"?q="+(this.currentUrl.city.location||location)
            +"&mode=" + this.currentUrl.mode+"&units=" + this.currentUrl.units
            + "&appid=" + this.currentUrl.appId;
    }

    private getWeather(location:string){
        //let sample:string = '{"coord":{"lon":-122.33,"lat":47.6},"weather":[{"id":804,"main":"Clouds","description":"overcast clouds","icon":"04d"}],"base":"stations","main":{"temp":51.6,"pressure":1022,"humidity":71,"temp_min":48.2,"temp_max":53.6},"visibility":16093,"wind":{"speed":5.82,"deg":190},"clouds":{"all":90},"dt":1525112100,"sys":{"type":1,"id":2949,"message":0.0048,"country":"US","sunrise":1525092742,"sunset":1525144881},"id":5809844,"name":"Seattle","cod":200}'

        let currentWeather:ICurrentWeatherJson;

        Jquery.get(this.buildCurrentUrl(location),(data:ICurrentWeatherJson)=>{
           currentWeather = data;

            this.setState((p,n)=>{
                return{
                    location: p.location,
                    currentTemp: Math.ceil(currentWeather.main.temp),
                    description: currentWeather.weather[0].description,
                    humidity: currentWeather.main.humidity
                }
            });

        });
    }

    public componentWillReceiveProps(nextProps){
        if(nextProps.location !== this.props.location){
            this.getWeather(nextProps.location)
        }
    }
    public render(){
        return(
            <article className="container currentForecast">
                <section className="row">
                    <div className="col-md-6 currentTemp text-right">
                       {this.state.currentTemp}&#176;
                    </div>
                    <div className="col-md-6 currentDescription">
                        <article className="container-fluid">
                            <section className="row">
                                <span className="unit col-md-12 text-left">F</span>
                            </section>
                            <section className="row">
                                <span className="description col-md-12 text-left">{this.state.description}</span>
                            </section>
                            <section className="row">
                                <span className="humidity col-md-12 text-left">{this.state.humidity}% Humidity</span>
                            </section>
                        </article>
                    </div>
                </section>
            </article>
        );

    }
}
