import * as React from 'react';
import * as Jquery from 'jquery';
import 'bootstrap/dist/js/bootstrap.js';
import './css/App.css';
import {CurrentWeather} from "./components/currentWeather";
import {Forecast} from "./components/forecast";


class App extends React.Component<any,any> {
    constructor(props: any) {
        super(props);
        this.state = {
            location: "Seattle, US"
        }
        this.searchCity.bind(this)
    }

    public searchCity =()=> {
        let city = Jquery("#citySearch").val();
        this.setState({
            location: city
        });

    }


    public render() {
        return (
            <article className="center">
                <img className="cloud1" src="./images/cloud1.png"/>
                <img className="cloud2" src="./images/cloud2.png"/>
                <div className="container weatherContainer">

                    <article className="row search">
                        <section className="col-md-1">
                            <i className="fa fa-search" onClick={this.searchCity}/>
                        </section>
                        <section className="col-md-10">
                            <input id="citySearch" placeholder={this.state.location}/>
                        </section>
                        <section className="col-md-1">
                            <i className="fa fa-close"/>
                        </section>
                        <CurrentWeather location={this.state.location}/>
                    </article>

                    <article className="row">
                        <Forecast location={this.state.location}/>
                    </article>
                </div>
            </article>
        );
    }
}

export default App;
